#!/bin/bash

# The purpose of this script is to provide a mean to easily connect to a remote Proxmox VM from the 
# command line, i.e. without having to access it from the Proxmox PVE UI.
#
# When run, after having loaded the configuration file provided by the user on CLI, this script 
# searches for the QEMU VM or LXC container the user is looking for in existing PVE nodes, then,
# when it's found, connects to it using `remote-viewer`.
#
# Read the whole documentation in the README.md file which is in the same directory as this script.
# It provides information about how to configure it and how to use it.

### Functions

# Basic INFO logging, always logged
function log(){
    echo "[i] $@";
}

# DEBUG logging, only logged if VERBOSE is set. Also allows to cat files in DEBUG mode
function log_debug(){
    if [[ ${VERBOSE} -eq 1 ]]; then
        if [ "$#" -eq 1 ]; then
            if [ -f "$@" ]; then
                echo "[d] File contents:";
                cat -s "$@" | sed "s/\\\\n/\n/g" | sed "s/^/\[d\]     /g";
            else
                echo "[d] $@";
            fi
        else
            echo "[d] $@";
        fi
    fi
}

# ERROR logging, logs to std.err
function log_error(){
    >&2 echo "[e] $@";
}

# Exit helper, logs an error the exits
function exit_error(){
    log_error "Exiting!";
    exit 1
}

# Loads environment variables from a file
function load_dotenv(){
  source <(cat $1 | sed -e '/^#/d;/^\s*$/d' -e "s/'/'\\\''/g" -e "s/=\(.*\)/='\1'/g")
}


### Main script

log "Starting PVE spice connection script..."

# configure script from .env file
if [ -z $1 ]; then
    log_error "Error: Missing mandatory argument: configuration filename"
    exit_error
else
    log "Loading environment variables from '$1' file"
    set -a
    if [ -f "$1" ]; then
        load_dotenv "$1"
        log_debug "Configuration file successfully loaded"
        log_debug "$1"
    else
        log_error "Error: configuration filename '$1' not found"
        exit_error
    fi
    set +a
fi

# if any of the mandatory environement variables is missing, exit with error
MISSING_CONFIGURATION=0
if [[ -z ${VMID} ]]; then
    log_error "Error: Missing VMID environment variable"
    MISSING_CONFIGURATION=1
fi
if [[ -z ${HOST} ]]; then
    log_error "Error: Missing HOST environment variable"
    MISSING_CONFIGURATION=1
fi
if [[ -z ${TOKEN} ]]; then
    log_error "Error: Missing TOKEN environment variable"
    MISSING_CONFIGURATION=1
fi
if [[ -z ${SECRET} ]]; then
    log_error "Error: Missing SECRET environment variable"
    MISSING_CONFIGURATION=1
fi
if [[ ${MISSING_CONFIGURATION} -eq 1 ]]; then
    exit_error
fi

# set default value for optional configuration if missing
if [[ -z ${PORT} ]]; then
    PORT=8006
fi
if [[ -z ${FULLSCREEN} ]]; then
    FULLSCREEN=0
fi
if [[ -z ${ENABLE_USB_REDIRECTION} ]]; then
    ENABLE_USB_REDIRECTION=0
fi
if [[ -z ${ENABLE_USB_AUTOSHARE} ]]; then
    ENABLE_USB_AUTOSHARE=0
fi
if [[ -z ${VERBOSE} ]]; then
    VERBOSE=0
fi
if [[ -z ${INSECURE} ]]; then
    INSECURE=1
fi

# display script configuration
log_debug "Configured the script as follows:"
log_debug "    Mandatory settings:"
log_debug "        VMID: '${VMID}'"
log_debug "        HOST: '${HOST}'"
log_debug "        TOKEN: '${TOKEN}'"
log_debug "        SECRET: '${SECRET}'"
log_debug "    Optional settings"
log_debug "        PORT: '${PORT}'"
log_debug "        VERBOSE: '${VERBOSE}'"
log_debug "        FULLSCREEN: '${FULLSCREEN}'"
log_debug "        ENABLE_USB_REDIRECTION: '${ENABLE_USB_REDIRECTION}'"
log_debug "        ENABLE_USB_AUTOSHARE: '${ENABLE_USB_AUTOSHARE}'"
log_debug "        INSECURE: '${INSECURE}'"
log_debug "    Special settings:"
log_debug "        CA_PATH: '${CA_PATH}' (mandatory if INSECURE is set to 0)"

if [[ ${VERBOSE} -eq 1 ]]; then
    CURL_VERBOSE=--verbose
fi

log "Building PVE spice connection..."

# if CA_PATH is set use it as curl certificate usage flag, else use --insecure to avoid certificate verification
if [[ ${INSECURE} -eq 1 ]]; then
    CERT_OPTION="--insecure"
    CERT_CONFIG=""
else if [[ ${CA_PATH} ]]; then
        CERT_OPTION="--capath"
        CERT_CONFIG="${CA_PATH}"
    fi
fi

log_debug "Using CERT_OPTION and CERT_CONFIG: '${CERT_OPTION} ${CERT_CONFIG}'"

# build auth header string
AUTHHEADER="Authorization: PVEAPIToken=${TOKEN}=${SECRET}"

log_debug "Using AUTHHEADER: '${AUTHHEADER}'"

# get auth response
AUTH_RESPONSE=`curl ${CURL_VERBOSE} -f -s -S ${CERT_OPTION} ${CERT_CONFIG} -H "${AUTHHEADER}" --url https://${HOST}:${PORT}/api2/json/nodes`

log_debug "Authenticating on '${HOST}:${PORT}'"
log_debug "Obtained AUTH_RESPONSE: ${AUTH_RESPONSE}"

NODES=`echo ${AUTH_RESPONSE} | jq -r ".[] | {nodes: .[].node}" | jq -r ".nodes"`

# Retrieve Proxmox node and VM type on which VMID is running
for NODE in ${NODES}
do
    # For each Proxmox node
    log_debug "Searching for VMID '${VMID}' on node '${NODE}'"

    # Search for VMID in QEMU vms
    QEMU_RESPONSE=`curl ${CURL_VERBOSE} -f -s -S ${CERT_OPTION} ${CERT_CONFIG} -H "${AUTHHEADER}" --url https://${HOST}:${PORT}/api2/json/nodes/${NODE}/qemu`
    if [[ ${QEMU_RESPONSE} ]]; then
        log_debug "Searching for VMID '${VMID}' in QEMU virtual machines of node '${NODE}'"
        log_debug "QEMU_RESPONSE: ${QEMU_RESPONSE}"
        QVMID=`echo ${QEMU_RESPONSE} | jq -r ".[] | {vmid: .[].vmid}" | jq -r ".vmid"`
        if [[ "${QVMID}" == *"${VMID}"* ]]; then
            NODENAME=${NODE}
            VMTYPE="qemu"
            break
        fi
    fi

    # Search for VMID in LXC containers
    LXC_RESPONSE=`curl ${CURL_VERBOSE} -f -s -S ${CERT_OPTION} ${CERT_CONFIG} -H "${AUTHHEADER}" --url https://${HOST}:${PORT}/api2/json/nodes/${NODE}/lxc`

    if [[ ${LXC_RESPONSE} ]]; then
        log_debug "Searching for VMID '${VMID}' in LXC containers of node '${NODE}'"
        log_debug "LXC_RESPONSE: ${LXC_RESPONSE}"
        LVMID=`echo ${LXC_RESPONSE} | jq -r ".[] | {vmid: .[].vmid}" | jq -r ".vmid"`
        if [[ "${LVMID}" == *"${VMID}"* ]]; then
            NODENAME=${NODE}
            VMTYPE="lxc"
            break
        fi
    fi
done

# if VM type could not be retrieved, exit with error message
if [[ -z ${VMTYPE} ]]; then
    log_error "Error: VMTYPE could not be detected!"
    log_error "    Are you sure that the ID of your VM or CT really is '${VMID}'?"
    log_error "    If yes, are you sure that you granted the correct permissions to '${TOKEN}' for '${VMID}' (i.e. token + role)?"
    log_error "    Double check and try again!"
    exit_error
fi

log_debug "VMID '${VMID}' with VMTYPE '${VMTYPE}' found on NODENAME: '${NODENAME}'"

# create tmp file for spice configuration
TMP=`mktemp`
curl ${CURL_VERBOSE} -f -s -S ${CERT_OPTION} ${CERT_CONFIG} -H "${AUTHHEADER}" --url https://${HOST}:${PORT}/api2/spiceconfig/nodes/${NODENAME}/${VMTYPE}/${VMID}/spiceproxy -d proxy=${HOST} > $TMP

# add custom virt-viewer options
tee -a $TMP <<EOF  > /dev/null
color-depth=32l
disable-effects=animation
enable-usbredir=$ENABLE_USB_REDIRECTION
enable-usb-autoshare=$ENABLE_USB_AUTOSHARE
usb-filter=-1,-1,-1,-1,1
fullscreen=$FULLSCREEN

EOF

# display resulting spice connection configuration
log_debug "Using spice connection configuration file:"
log_debug $TMP

log "Connecting remotely to VMID '${VMID}' on '${HOST}:${PORT}'"

# launch remote-viewer in background
remote-viewer $TMP &

log "Remote viewer launched successfully."

